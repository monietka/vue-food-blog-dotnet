import Vue from 'vue';
import Vuex from 'vuex';
import router from './router';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        appTitle: 'EatIT',
        user: null,
        error: null,
        loading: false
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
        },
        setError(state, payload) {
            state.error = payload;
        },
        setLoading(state, payload) {
            state.loading = payload;
        }
    },
    actions: {
        userSignUp({ commit }, payload) {
            commit('setLoading', true);
            firebase
                .auth()
                .createUserWithEmailAndPassword(payload.email, payload.password)
                .then(firebaseUser => {
                    commit('setUser', {
                        email: firebaseUser.user.email
                    });
                    commit('setLoading', false);
                    router.push('/');
                })
                .catch(error => {
                    commit('setError', error.message);
                    commit('setLoading', false);
                });
        },
        userSignIn({ commit }, payload) {
            commit('setLoading', true);
            firebase
                .auth()
                .signInWithEmailAndPassword(payload.email, payload.password)
                .then(firebaseUser => {
                    commit('setUser', {
                        email: firebaseUser.user.email
                    });
                    commit('setLoading', false);
                    commit('setError', null);
                    router.push('/');
                })
                .catch(error => {
                    commit('setError', error.message);
                    commit('setLoading', false);
                });
        },
        autoSignIn({ commit }, payload) {
            commit('setUser', { email: payload.email });
        },
        signUserInGoogle({ commit }) {
            commit('setLoading', true);
            firebase
                .auth()
                .signInWithPopup(new firebase.auth.GoogleAuthProvider())
                .then(firebaseUser => {
                    const newUser = {
                        id: firebaseUser.uid,
                        name: firebaseUser.displayName,
                        email: firebaseUser.email,
                        photoUrl: firebaseUser.photoURL
                    };
                    commit('setUser', newUser);
                    commit('setLoading', false);
                    commit('setError', null);
                    router.push('/');
                })
                .catch(error => {
                    commit('setLoading', false);
                    commit('setError', error.message);
                    console.log(error);
                });
        },
        userSignOut({ commit }) {
            firebase.auth().signOut();
            commit('setUser', null);
            alert('Signed out!!');
            router.push('/');
        }
    },
    getters: {
        isAuthenticated(state) {
            return state.user !== null && state.user !== undefined;
        }
    }
});
