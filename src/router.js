import Vue from 'vue';
import Router from 'vue-router';


const routerOptions = [
    {
        path: '/',
        name: 'home',
        component: 'Home'
    },
    {
        path: '/signin',
        name: 'signin',
        component: 'SignIn'
    },
    {
        path: '/signup',
        name: 'signup',
        component: 'SignUp'
    },
    {
        path: '/about',
        name: 'about',
        component: 'About'
    },
    {
        path: '/recipes',
        name: 'recipes',
        component: 'Recipes'
    },
    {
        path: '/profile',
        name: 'profile',
        component: 'Profile',
        //meta: { requiresAuth: true }
    },
    { path: '*', component: 'NotFound' }
];

const routes = routerOptions.map(route => {
    return {
        ...route,
        component: () => import(`@/views/${route.component}.vue`)
    };
});

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes
});

//router.beforeEach((to, from, next) => {
//    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
//    const isAuthenticated = firebase.auth().currentUser;
//    if (requiresAuth && !isAuthenticated) {
//        next('/signin');
//    } else {
//        next();
//    }
//});

export default router;
